import React from 'react';
import PropTypes from 'prop-types';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {

    /*courseProp deconstructed*/
    const {_id, name, description, price} = courseProp;


    /*Use the state hook for this component to be able to store its state*/
    /*State are used to keep track of information related to individual components*/
    /*Syntax:
        const [getter, setter] = useState(initialGetterValue);
    */
    // const [count, setCount] = useState(0);
    // console.log(useState(0));
    // const [seat, setSeat] = useState(30);

    /*Function that keeps track of enrollees for a course*/
    // function enroll(){
    //     if (seat > 0){
    //         setCount(count + 1);
    //         setSeat(seat - 1);
    //     }
    //     else{
    //         alert ('No more seats');
    //     }
    // }

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                {/*course = data*/}
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Button variant="primary" to={`/coursesView/${_id}`}>Details</Button>

            </Card.Body>
        </Card>
    )
}


// Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
